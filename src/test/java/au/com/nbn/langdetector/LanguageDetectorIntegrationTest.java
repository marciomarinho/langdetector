package au.com.nbn.langdetector;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.FlashMap;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class LanguageDetectorIntegrationTest {

    public static final String TXT_EXTENSION = ".txt";
    public static final String LANGUAGES = "languages";
    public static final String LANGUAGES_FOLDER = "/languages";

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldDetectEnglishLanguageFromFileContent() throws Exception {

        MockMultipartFile multipartFile = new MockMultipartFile("file", "english_article.txt",
                "text/plain", IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("english_article.txt")));
        FlashMap flashMap = this.mvc.perform(multipart("/").file(multipartFile))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/"))
                .andExpect(flash().attributeExists("message"))
                .andReturn().getFlashMap();

        assertEquals(flashMap.get("message"), "The file content's language is english !");
    }

    @Test
    public void shouldDetectAllSupportedLanguages() throws Exception {

        URL url = getClass().getResource(LANGUAGES_FOLDER);
        Path path = Paths.get(url.toURI());

        List<String> supportedLanguagesFileList = Files.walk(path)
                .map(p -> p.getFileName().toString())
                .filter(n -> !n.equals(LANGUAGES))
                .collect(Collectors.toList());

        for (String languageFileName : supportedLanguagesFileList) {

            final String languageName = languageFileName.substring(0, languageFileName.indexOf(TXT_EXTENSION));
            final MockMultipartFile multipartFile = new MockMultipartFile("file", languageName.concat("_article.txt"),
                    "text/plain", IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream(languageName.concat("_article.txt"))));
            FlashMap flashMap = this.mvc.perform(multipart("/").file(multipartFile))
                    .andExpect(status().isFound())
                    .andExpect(header().string("Location", "/"))
                    .andExpect(flash().attributeExists("message"))
                    .andReturn().getFlashMap();

            assertEquals(flashMap.get("message"), "The file content's language is ".concat(languageName).concat(" !"));

        }

    }

    @Test
    public void shouldNotDetectRussianFromFileContentAsItIsNotSupported() throws Exception {

        MockMultipartFile multipartFile = new MockMultipartFile("file", "russian_article.txt",
                "text/plain", IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("russian_article.txt")));
        FlashMap flashMap = this.mvc.perform(multipart("/").file(multipartFile))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/"))
                .andExpect(flash().attributeExists("message"))
                .andReturn().getFlashMap();

        assertEquals(flashMap.get("message"), "The file content's language is *NOT DETECTED* !");
    }

    @Test
    public void shouldNotDetectNorwegianFromFileContentAsItIsNotSupported() throws Exception {

        MockMultipartFile multipartFile = new MockMultipartFile("file", "norwegian_article.txt",
                "text/plain", IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("norwegian_article.txt")));
        FlashMap flashMap = this.mvc.perform(multipart("/").file(multipartFile))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/"))
                .andExpect(flash().attributeExists("message"))
                .andReturn().getFlashMap();

        assertEquals(flashMap.get("message"), "The file content's language is *NOT DETECTED* !");
    }

    @Test
    public void shouldReturnAnErrorAsFileIsEitherNullOrEmpty() throws Exception {

        MockMultipartFile multipartFile = new MockMultipartFile("file", "norwegian_article.txt",
                "text/plain", "".getBytes());

        FlashMap flashMap = this.mvc.perform(multipart("/").file(multipartFile))
                .andExpect(status().isFound())
                .andExpect(header().string("Location", "/"))
                .andExpect(flash().attributeExists("error"))
                .andReturn().getFlashMap();

        assertEquals(flashMap.get("error"), "Nice try, but you got an error as the file cannot be null or empty!");
    }

}

