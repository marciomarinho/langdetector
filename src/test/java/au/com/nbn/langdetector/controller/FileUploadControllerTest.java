package au.com.nbn.langdetector.controller;

import au.com.nbn.langdetector.service.LanguageDetectionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static au.com.nbn.langdetector.service.LanguageDetectionService.NOT_DETECTED;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class FileUploadControllerTest {

    private static final Set<String> availableLanguages = new HashSet<>(Arrays.asList(
            new String[]{"swedish", "german", "spanish", "danish",
                    "english", "albanian", "italian", "portuguese",
                    "polish", "french", "afrikaans", "czech"}));

    public static final String ERROR_FILE_CANNOT_BE_NULL_OR_EMPTY = "Nice try, but you got an error as the file cannot be null or empty!";

    @Mock
    private LanguageDetectionService languageDetectionService;

    @Mock
    private Model model;

    @Mock
    private RedirectAttributes redirectAttributes;

    private FileUploadController fileUploadController;

    @Before
    public void setup() {
        given(languageDetectionService.getAllSupportedLanguages()).willReturn(availableLanguages);
        fileUploadController = new FileUploadController(languageDetectionService);
    }

    @Test
    public void shouldReturnAvailableLanguages() {
        assertEquals(fileUploadController.listUploadedFiles(model), "file-upload");
        verify(model).addAttribute("languages", availableLanguages);
    }

    @Test
    public void shouldDetectLanguageInFileContent() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "english_article.txt",
                "text/plain", "Something new is happening here".getBytes());

        given(languageDetectionService.detect(file)).willReturn("english");
        assertEquals(fileUploadController.handleFileUpload(file, redirectAttributes), "redirect:/");
        verify(redirectAttributes).addFlashAttribute("message", "The file content's language is english !");
        verify(redirectAttributes).addFlashAttribute("wasDetected", true);
    }

    @Test
    public void shouldNotDetectLanguageInFileContent() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "english_article.txt",
                "text/plain", "Something new is happening here".getBytes());

        given(languageDetectionService.detect(file)).willReturn(NOT_DETECTED);
        assertEquals(fileUploadController.handleFileUpload(file, redirectAttributes), "redirect:/");
        verify(redirectAttributes).addFlashAttribute("message", "The file content's language is *NOT DETECTED* !");
        verify(redirectAttributes).addFlashAttribute("wasDetected", false);
    }

    @Test
    public void shouldReturnErrorDueToNullFile() throws IOException {
        assertEquals(fileUploadController.handleFileUpload(null, redirectAttributes), "redirect:/");
        verify(redirectAttributes).addFlashAttribute("error", ERROR_FILE_CANNOT_BE_NULL_OR_EMPTY);
    }

    @Test
    public void shouldReturnErrorDueToEmptyFile() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "english_article.txt",
                "text/plain", "".getBytes());
        assertEquals(fileUploadController.handleFileUpload(file, redirectAttributes), "redirect:/");
        verify(redirectAttributes).addFlashAttribute("error", ERROR_FILE_CANNOT_BE_NULL_OR_EMPTY);
    }

    @Test(expected = IOException.class)
    public void shouldThrowExceptionWhenTryingToReadFileBytes() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "english_article.txt",
                "text/plain", "Something new is happening here".getBytes());

        given(languageDetectionService.detect(file)).willThrow(new IOException("Well, it is a bad file..."));
        fileUploadController.handleFileUpload(file, redirectAttributes);
    }

}