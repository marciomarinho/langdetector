package au.com.nbn.langdetector.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class FileUploadErrorControllerTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private Throwable ex;

    private FileUploadErrorController fileUploadErrorController;

    @Before
    public void setup() {
        fileUploadErrorController = new FileUploadErrorController();
    }

    @Test
    public void shouldReturnErrorPage() {
        given(ex.getMessage()).willReturn("File limit exceeded.");
        assertEquals(fileUploadErrorController.handleFileException(request, ex), "file-upload-limit-error.html");
    }

}