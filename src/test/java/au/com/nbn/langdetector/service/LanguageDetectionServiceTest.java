package au.com.nbn.langdetector.service;

import au.com.nbn.langdetector.config.LanguageDetectionConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static au.com.nbn.langdetector.service.LanguageDetectionService.NOT_DETECTED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class LanguageDetectionServiceTest {

    private static final Set<String> availableLanguages = new HashSet<>(Arrays.asList(
            new String[]{"swedish", "german", "spanish", "danish",
                    "english", "albanian", "italian", "portuguese",
                    "polish", "french", "afrikaans", "czech"}));

    private static final int matchingRatioOverOtherLanguages = 20;
    private static final int languageDictionaryRatio = 50;
    private static final String relaxRatioPercentageForLanguage = "english";
    private static final Set<String> relaxRatioPercentageForVerySimilarLanguages = new HashSet<String>() {{
        add("portuguese");
        add("spanish");
    }};

    @Mock
    private DictionaryService dictionaryService;

    @Mock
    private LanguageDetectionConfig languageDetectionConfig;

    @Mock
    private MultipartFile multipartFileWithIOException;

    private LanguageDetectionService languageDetectionService;

    @Before
    public void setup() {
        languageDetectionService = new LanguageDetectionService(dictionaryService, languageDetectionConfig);
        given(languageDetectionConfig.getMatchingRatioOverOtherLanguages()).willReturn(matchingRatioOverOtherLanguages);
        given(languageDetectionConfig.getLanguageDictionaryRatio()).willReturn(languageDictionaryRatio);
        given(languageDetectionConfig.getRelaxRatioPercentageForLanguage()).willReturn(relaxRatioPercentageForLanguage);
        given(languageDetectionConfig.getRelaxRatioPercentageForVerySimilarLanguages()).willReturn(relaxRatioPercentageForVerySimilarLanguages);
    }

    @Test
    public void shouldGetAllSupportedLanguages() {
        given(dictionaryService.getAllSupportedLanguages()).willReturn(availableLanguages);
        assertTrue(languageDetectionService.getAllSupportedLanguages().containsAll(availableLanguages));
        verify(dictionaryService).getAllSupportedLanguages();
    }

    @Test
    public void shouldDetectEnglishLanguage() throws IOException {

        MockMultipartFile file = new MockMultipartFile("file", "english_article.txt",
                "text/plain", "Something new is happening here".getBytes());

        given(dictionaryService.getAllSupportedLanguages()).willReturn(new HashSet<String>() {{
            add("english");
            add("swedish");
        }});

        given(dictionaryService.doesLanguageContainWord("english", "something")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "is")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "here")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "happening")).willReturn(true);

        assertEquals(languageDetectionService.detect(file), "english");

    }

    @Test
    public void shouldDetectGermanLanguage() throws IOException {

        MockMultipartFile file = new MockMultipartFile("file", "german_article.txt",
                "text/plain", "deine wissen müssen wirklich soll nie weg hey kein des würde einfach leben am viel".getBytes());

        given(dictionaryService.getAllSupportedLanguages()).willReturn(new HashSet<String>() {{
            add("german");
            add("english");
            add("swedish");
        }});

        given(dictionaryService.doesLanguageContainWord("german", "deine")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "wissen")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "soll")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "nie")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "weg")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "hey")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "einfach")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "leben")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("english", "des")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "viel")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "am")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("swedish", "deine")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "soll")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "würde")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "leben")).willReturn(true);

        assertEquals(languageDetectionService.detect(file), "german");

    }

    @Test
    public void shouldNotDetectPolishAsItDoesNotReachDesiredRatioOverOtherLanguages() throws IOException {

        MockMultipartFile file = new MockMultipartFile("file", "polish_article.txt",
                "text/plain", "deine wissen müssen wirklich soll nie weg hey kein des würde einfach leben am viel".getBytes());

        given(dictionaryService.getAllSupportedLanguages()).willReturn(new HashSet<String>() {{
            add("german");
            add("english");
            add("swedish");
            add("polish");
        }});

        given(dictionaryService.doesLanguageContainWord("polish", "wissen")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "soll")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "nie")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "weg")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "hey")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "einfach")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("polish", "leben")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("german", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "soll")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "nie")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "weg")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "hey")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "einfach")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("german", "leben")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("english", "des")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "viel")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "am")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("swedish", "deine")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "soll")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "würde")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "leben")).willReturn(true);

        assertEquals(languageDetectionService.detect(file), NOT_DETECTED);

    }

    @Test
    public void shouldNotDetectCzechAsItDoesNotReachDesiredRatioOverDictionaryMatches() throws IOException {

        MockMultipartFile file = new MockMultipartFile("file", "czech_article.txt",
                "text/plain", "deine wissen müssen wirklich soll nie weg hey kein des würde einfach leben am viel".getBytes());

        given(dictionaryService.getAllSupportedLanguages()).willReturn(new HashSet<String>() {{
            add("german");
            add("english");
            add("swedish");
            add("czech");
        }});

        given(dictionaryService.doesLanguageContainWord("czech", "wissen")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("czech", "wirklich")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("czech", "soll")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("polish", "nie")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("german", "wirklich")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("english", "des")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("english", "viel")).willReturn(true);

        given(dictionaryService.doesLanguageContainWord("swedish", "deine")).willReturn(true);
        given(dictionaryService.doesLanguageContainWord("swedish", "wirklich")).willReturn(true);

        assertEquals(languageDetectionService.detect(file), NOT_DETECTED);

    }

    @Test
    public void shouldNotDetectNorwegianAsItIsNotSupported() throws IOException {

        MockMultipartFile file = new MockMultipartFile("file", "german_article.txt",
                "text/plain", "deine wissen müssen wirklich soll nie weg hey kein des würde einfach leben am viel".getBytes());

        given(dictionaryService.getAllSupportedLanguages()).willReturn(new HashSet<String>() {{
            add("german");
            add("english");
            add("swedish");
            add("polish");
        }});

        assertEquals(languageDetectionService.detect(file), NOT_DETECTED);

    }

    @Test(expected = IOException.class)
    public void shouldThrowExceptionWhenTryingToReadFileBytes() throws Exception {
        when(multipartFileWithIOException.getBytes()).thenThrow(new IOException("Well, it is a bad file..."));
        languageDetectionService.detect(multipartFileWithIOException);
    }
}