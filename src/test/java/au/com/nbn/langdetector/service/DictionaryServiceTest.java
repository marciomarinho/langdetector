package au.com.nbn.langdetector.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.Is.is;

public class DictionaryServiceTest {

    private DictionaryService dictionaryService;

    private static final List<String> availableLanguages = Arrays.asList(
            new String[]{"swedish", "german", "spanish", "danish",
                    "english", "albanian", "italian", "portuguese",
                    "polish", "french", "afrikaans", "czech"});

    @Before
    public void setup() throws IOException {
        dictionaryService = new DictionaryService();
    }

    @Test
    public void shouldContainAllSupportedLanguages() {
        Set<String> expected = new HashSet<>(availableLanguages);
        assertTrue(dictionaryService.getAllSupportedLanguages().containsAll(expected));
    }

    @Test
    public void shouldContainWordsForSupportedLanguagesInDictionary() {
        availableLanguages.stream()
                .forEach(e -> assertTrue(dictionaryService.numberOfWordsInLanguageSet(e) > 0));
    }

    @Test
    public void shouldReturnMinusOneForNumberOfWordsWhenLanguageIsNotSupported() {
        Assert.assertThat(dictionaryService.numberOfWordsInLanguageSet("nigerian"), is(-1));
    }

    @Test
    public void shouldReturnFalseWhenLanguageIsNotSupported() {
        Assert.assertThat(dictionaryService.doesLanguageContainWord("something", "abcd"), is(false));
    }

}