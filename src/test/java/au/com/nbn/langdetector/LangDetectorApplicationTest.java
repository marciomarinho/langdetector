package au.com.nbn.langdetector;

import org.junit.Test;

/**
 * Simply to make coverage happy, which made me unhappy,
 * but life is usually a trade off. :-)
 */
public class LangDetectorApplicationTest {

    @Test
    public void shouldRunSpringApplication() {
        LangDetectorApplication.main(new String[]{});
    }

}