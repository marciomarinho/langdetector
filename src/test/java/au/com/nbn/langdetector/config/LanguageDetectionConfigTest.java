package au.com.nbn.langdetector.config;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LanguageDetectionConfigTest {

    private LanguageDetectionConfig languageDetectionConfig;

    @Before
    public void setup() {
        languageDetectionConfig = new LanguageDetectionConfig(20, 50,
                "english", new HashSet<String>() {{
            add("porguguese");
            add("spanish");
        }});
    }

    @Test
    public void shouldReturnCorretValuesForConfiguration() {
        Set<String> expected = new HashSet<String>() {{
            add("porguguese");
            add("spanish");

        }};

        assertEquals(languageDetectionConfig.getMatchingRatioOverOtherLanguages(), 20);
        assertEquals(languageDetectionConfig.getLanguageDictionaryRatio(), 50);
        assertEquals(languageDetectionConfig.getRelaxRatioPercentageForLanguage(), "english");
        assertTrue(expected.containsAll(languageDetectionConfig.getRelaxRatioPercentageForVerySimilarLanguages()));
    }
}
