package au.com.nbn.langdetector.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class FileUploadErrorController {

    @ExceptionHandler(MultipartException.class)
    String handleFileException(HttpServletRequest request, Throwable ex) {
        return "file-upload-limit-error.html";
    }

}
