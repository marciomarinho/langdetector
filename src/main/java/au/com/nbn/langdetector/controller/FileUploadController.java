package au.com.nbn.langdetector.controller;

import au.com.nbn.langdetector.service.LanguageDetectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

import static au.com.nbn.langdetector.service.LanguageDetectionService.NOT_DETECTED;
import static java.util.Objects.isNull;

@Controller
public class FileUploadController {

    private LanguageDetectionService languageDetectionService;

    @Autowired
    public FileUploadController(LanguageDetectionService languageDetectionService) {
        this.languageDetectionService = languageDetectionService;
    }

    @GetMapping("/")
    public String listUploadedFiles(Model model) {
        model.addAttribute("languages", languageDetectionService.getAllSupportedLanguages());
        return "file-upload";
    }

    @PostMapping("/")
    public String handleFileUpload(@RequestParam(value = "file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) throws IOException {

        if (isNull(file) || file.isEmpty()) {
            redirectAttributes.addFlashAttribute("error", "Nice try, but you got an error as the file cannot be null or empty!");
            return "redirect:/";
        }

        String analysisResult = languageDetectionService.detect(file);
        boolean wasDetected = !analysisResult.equals(NOT_DETECTED);
        redirectAttributes.addFlashAttribute("message", "The file content's language is " + analysisResult + " !");
        redirectAttributes.addFlashAttribute("wasDetected", wasDetected);

        return "redirect:/";
    }
}
