package au.com.nbn.langdetector.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@Getter
public class LanguageDetectionConfig {

    private final int matchingRatioOverOtherLanguages;
    private final int languageDictionaryRatio;
    private final String relaxRatioPercentageForLanguage;
    private final Set<String> relaxRatioPercentageForVerySimilarLanguages;

    public LanguageDetectionConfig(@Value("${frequency.ratio.percentage}") final int matchingRatioOverOtherLanguages,
                                   @Value("${language.dictionary.ratio.percentage}") final int languageDictionaryRatio,
                                   @Value("${relax.ratio.percentage.for.language}") final String relaxRatioPercentageForLanguage,
                                   @Value("${relax.ratio.percentage.for.very.similar.languages}") final Set<String> relaxRatioPercentageForVerySimilarLanguages) {
        this.matchingRatioOverOtherLanguages = matchingRatioOverOtherLanguages;
        this.languageDictionaryRatio = languageDictionaryRatio;
        this.relaxRatioPercentageForLanguage = relaxRatioPercentageForLanguage;
        this.relaxRatioPercentageForVerySimilarLanguages = relaxRatioPercentageForVerySimilarLanguages;
    }


}
