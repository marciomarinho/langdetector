package au.com.nbn.langdetector.service;

import au.com.nbn.langdetector.config.LanguageDetectionConfig;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
public class LanguageDetectionService {

    public static final String NOT_DETECTED = "*NOT DETECTED*";

    private final DictionaryService dictionaryService;

    private final LanguageDetectionConfig languageDetectionConfig;

    @Autowired
    public LanguageDetectionService(DictionaryService dictionaryService,
                                    LanguageDetectionConfig languageDetectionConfig) {
        this.dictionaryService = dictionaryService;
        this.languageDetectionConfig = languageDetectionConfig;
    }

    public Set<String> getAllSupportedLanguages() {
        return dictionaryService.getAllSupportedLanguages();
    }

    public String detect(final MultipartFile file) throws IOException {

        byte[] bytes = file.getBytes();
        String textForAnalysis = new String(bytes);

        String[] words = textForAnalysis.split(" ");
        Map<String, Integer> wordsFrequency = new HashMap<>();

        for (String language : dictionaryService.getAllSupportedLanguages()) {

            int hitsCounter = 0;

            for (String word : words) {
                word = cleanPunctuationWhenFound(word);

                if (dictionaryService.doesLanguageContainWord(language, word.toLowerCase())) {
                    hitsCounter++;
                }

            }

            wordsFrequency.put(language, hitsCounter);

        }

        return getHighestFrequencyLanguage(wordsFrequency, words.length);

    }

    private String cleanPunctuationWhenFound(String word) {
        if (word.contains(".") || word.contains(",") || word.contains(";") || word.contains(":") || word.contains("-")) {
            return word.replace(".", "")
                    .replace(",", "")
                    .replace(";", "")
                    .replace(":", "")
                    .replace("-", "");
        }
        return word;
    }


    private String getHighestFrequencyLanguage(final Map<String, Integer> wordsFrequency, final int wordsCount) {

        Pair<String, Integer> highestFrequency = getHighestFrequency(wordsFrequency);

        if (isEmpty(highestFrequency.getKey())) {
            return NOT_DETECTED;
        }

        if (!languageDetectionConfig.getRelaxRatioPercentageForLanguage().equals(highestFrequency.getKey())) {

            if (highestFrequency.getValue() < (wordsCount * languageDetectionConfig.getLanguageDictionaryRatio()) / 100) {
                return NOT_DETECTED;
            }

            if (!languageDetectionConfig.getRelaxRatioPercentageForVerySimilarLanguages().contains(highestFrequency.getKey())) {
                return checkMatchRatioOverOtherLanguages(highestFrequency.getKey(), wordsFrequency);
            }
        }

        return highestFrequency.getKey();
    }

    private Pair<String, Integer> getHighestFrequency(final Map<String, Integer> wordsFrequency) {

        int frequency = 0;
        String language = "";

        for (Map.Entry<String, Integer> entry : wordsFrequency.entrySet()) {

            if (entry.getValue() > frequency) {
                frequency = entry.getValue();
                language = entry.getKey();
            }
        }

        return new ImmutablePair<>(language, frequency);
    }

    private String checkMatchRatioOverOtherLanguages(final String language, final Map<String, Integer> wordsFrequency) {

        int winnerFrequency = wordsFrequency.get(language);

        for (Map.Entry<String, Integer> entry : wordsFrequency.entrySet()) {

            if (entry.getKey().equals(language)) {
                continue;
            }

            int rationInRelationToWinning = (entry.getValue() + ((entry.getValue() * languageDetectionConfig.getMatchingRatioOverOtherLanguages()) / 100));

            if (winnerFrequency <= rationInRelationToWinning) {
                return NOT_DETECTED;
            }

        }

        return language;

    }

}
