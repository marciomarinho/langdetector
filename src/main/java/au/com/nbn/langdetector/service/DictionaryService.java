package au.com.nbn.langdetector.service;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

import static org.springframework.util.CollectionUtils.isEmpty;

@Component
class DictionaryService {

    public static final String TXT_EXTENSION = ".txt";
    public static final String LANGUAGES_FOLDER = "/languages";
    private Map<String, Set<String>> languages;

    public DictionaryService() throws IOException {
        languages = new HashMap<>();

        for (Resource languageResource : getResourceFiles(LANGUAGES_FOLDER)) {

            Set<String> words = new HashSet<>();
            String language = IOUtils.toString(languageResource.getInputStream(), "UTF-8");

            for (String word : language.split("\n")) {
                words.add(word.toLowerCase());
            }

            languages.put(languageResource.getFilename().substring(0, languageResource.getFilename().indexOf(TXT_EXTENSION)), words);

        }

    }

    Set<String> getAllSupportedLanguages() {
        return Collections.unmodifiableSet(languages.keySet());
    }

    boolean doesLanguageContainWord(final String language, final String word) {
        Objects.requireNonNull(language);
        Objects.requireNonNull(word);
        if (word.length() < 3) return false;
        Set<String> wordsForSelectedLanguage = languages.get(language);
        if (isEmpty(wordsForSelectedLanguage)) return false;
        return wordsForSelectedLanguage.contains(word);
    }

    int numberOfWordsInLanguageSet(final String language) {
        Objects.requireNonNull(language);
        Set<String> wordsForSelectedLanguage = languages.get(language);
        if (isEmpty(wordsForSelectedLanguage)) return -1;
        return wordsForSelectedLanguage.size();
    }

    private List<Resource> getResourceFiles(String path) throws IOException {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        List<Resource> result = new ArrayList<>();
        Resource[] resources = resolver.getResources("classpath*:/languages/*.txt");
        for (Resource resource : resources) {
            result.add(resource);
        }
        return result;
    }

}
