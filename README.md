# Welcome to LANG-DETECTOR!
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

LangDetector is a web application that detects human languages inspecting text files.

  - Just run the application via your preferred **IDE**, or you can also run it using **maven** :
  - mvn clean install
  - java -jar target/langdetector-0.0.1-SNAPSHOT.jar
  - Go to your browser and hit : http://localhost:8080
  - Upload a non-empty text file to be analyzed.

## What does it do?
Our software detects 12 different languages, including :
  - swedish
  - german
  - spanish
  - danish
  - english
  - albanian
  - italian
  - portuguese
  - polish
  - french
  - afrikaans
  - czech

## Awesome, isn't ? but how do we do that ?
We implement an "advanced" human language detection algorithm, as we take in account similarities between languages, and dictionaries. In order to make things as precise as possible, we have the following steps when checking for languages:

- We count words frequencies based on all our dictionaries.
- We have a special rule for the English Language, as it is a kind of platypus, and was built stealing features from other languages, so it usually mismatches, but we relaxed the algorithm to account for that!
- Our algorithm checks the ratio percentage match when comparing the languages frequency matched. For example, when analysing a text in Swedish we find a lot of similarities with German, so, in order to find a winner, the winner ratio must be at least 20% higher than all other languages, unless it is English. We also check for dictionary matches, and a winner language must have at least 50% of its words matched against the dictionary.
- We also account for **very similar languages**, such as ***portuguese and spanish***, as the winner usually wins by a short margin.

It is all about configuration ( application.properties ):

- frequency.ratio.percentage=20
- language.dictionary.ratio.percentage=50
- relax.ratio.percentage.for.language=english
- relax.ratio.percentage.for.very.similar.languages=portuguese,spanish
- Supporting a new language is just a matter of adding a new dictionary, and possibly adjusting the ratios.

## It is not over...

We could, but we did not stop there, we also have :

- Unit tests for each class/feature.
- Integration tests for every supported language and also for not supported ones.
- All tests are backed by articles files taken from some news website written in the target language. You can find those files inside /src/test/resources/*.txt
  
### Tech Stack

We have used the following tech stack to build this solution:

- [Java8] - Awesome language!
- [SpringBoot] - Makes a developer's life really easy.
- [SpringFramework] - No words needed for this beauty.
- [Twitter Bootstrap] - great UI boilerplate for modern web apps
- [Mockito] - Marvelous mock test library.
- [IOUtils] - For helping on I/O related stuff.

### Built by Marcio Marinho.

### Enjoy it!